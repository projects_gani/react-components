import PropTypes from 'prop-types';
import './Main.css';
import './Header.css';
import './Footer.css';

import data from './data';

function Header({title = "Header defualt title"}) {
  return (
    <div className = "Header">
      <h1>{title}</h1>
    </div>
  );
}

Header.propTypes = {
  title: PropTypes.string
}

function Main() {
  return (
    <div className = "Main">
      <h1>Main</h1>
      <Photo>
        <ul>
          {data.slice(0, 10).map((img) => {
            return (
              <li id = {img.id} key={img.id}>
                <h3>{img.title}</h3>
                <img alt={img.title} src={img.url}/>
              </li>
            );
          })}
        </ul>
    </Photo>
    </div>
  );
}

function Photo(props) {
  return (
    props.children
  );
}

function Footer({title = "Footer defualt title"}) {
  return (
    <div className = "Footer">
      <h1>{title}</h1>
    </div>
  );
}

function Page() {
  return (
    <>
      <Header title = {"Header title"}/>
      <Main />
      <Footer />
    </>
  );
}

export default Page;
